function [ F ] = sample_factors_scores( Y_tilde, X, Z_1,Lambda,resid_Y_prec,F_b,F_a,F_h2 )
%Sample factor scores given factor loadings (F_a), factor heritabilities (F_h2) and
%phenotype residuals
    Lmsg = bsxfun(@times,Lambda,resid_Y_prec);
    tau_e = 1./(1-F_h2);
    S=chol(Lambda'*Lmsg+diag(tau_e),'lower');
    Meta = ((Y_tilde*Lmsg + bsxfun(@times,X*F_b + Z_1*F_a,tau_e'))/S')/S;
    F = Meta + randn(size(Meta))/S;   
end

