function [F_a] = sample_F_a(F,X_f,Z_1,F_h2,invert_aPXA_bDesignDesignT)
%samples genetic effects on factors (F_a) conditional on the factor scores F:
% F_i = F_{a_i} + E_i, E_i~N(0,s2*(1-h2)*I) for each latent trait i
% U_i = zeros(r,1) if h2_i = 0
% it is assumed that s2 = 1 because this scaling factor is absorbed in
% Lambda
% invert_aZZt_Ainv has parameters to diagonalize a*Z_1*Z_1' + b*I for fast
% inversion:



U = invert_aPXA_bDesignDesignT.U;
s1 = invert_aPXA_bDesignDesignT.s1;
s2 = invert_aPXA_bDesignDesignT.s2;
Design_U = invert_aPXA_bDesignDesignT.Design_U;

k = size(F,2);
n = size(Z_1,2);
b_f = size(X_f,2);
XtX = X_f'*X_f;
tau_e = 1./(1-F_h2);
tau_u = 1./F_h2;
b = U'*Z_1'*bsxfun(@times,F,tau_e');
z = randn(n,k);
F_a = zeros(n,k);
for j=1:k
    if tau_e(j)==1
%         size(F_a)
%         size(X_f'*F(:,j).*tau_e(j))
%         chol_XtX*randn(b_f,1)
% %         size(chol_XtX*randn(b_f,1))
        Cov = XtX*tau_e(j);
        F_a(:,j) = [Cov\(X_f'*F(:,j).*tau_e(j)) + chol(Cov)\randn(b_f,1);zeros(n-b_f,1)];
    elseif tau_e(j) == Inf
        F_a(:,j) = F(:,j);
    else
        d = s1*tau_u(j) + s2*tau_e(j);
        mlam = bsxfun(@times,b(:,j),1./d);
        F_a(:,j) = U*(mlam + bsxfun(@times,z(:,j),1./sqrt(d)));
    end
    if tau_e(j) == 1
%         disp(F_a(:,j))
    end
end
end



